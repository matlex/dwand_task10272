import re
import json
import html
from collections import OrderedDict


class JSONtoHTMLConverter:
    def __init__(self, source_file=None):
        self.output = ""
        if source_file:
            self.source_file = source_file
        else:
            self.source_file = "source.json"

    def extract_classes_from_tag(self, tag):
        class_pattern = '\.([\w-]+)'
        # TODO Here we can add try:except statements
        found_classes = re.findall(class_pattern, tag)
        return found_classes

    def extract_ids_from_tag(self, tag):
        id_pattern = '#([\w-]+)'
        # TODO Here we can add try:except statements
        found_ids = re.findall(id_pattern, tag)
        return found_ids

    def extract_html_tag_from_tag_key(self, tag):
        id_pattern = '([A-Za-z0-9-]+)'
        # TODO Here we can add try:except statements
        found_tag = re.search(id_pattern, tag).group(1)
        return found_tag

    def format_string_with_open_and_close_tags(self, tag, item):
        found_classes = self.extract_classes_from_tag(tag)
        found_ids = self.extract_ids_from_tag(tag)
        html_tag = self.extract_html_tag_from_tag_key(tag)

        if found_classes and found_ids:
            if item:
                return '<{open_tag} id="{ids}" class="{classes}">{value}</{close_tag}>'.format(
                    open_tag=html_tag,
                    value=html.escape(item),
                    close_tag=html_tag,
                    classes=" ".join(found_classes),
                    ids=" ".join(found_ids)
                )
            else:
                return '<{open_tag} id="{ids}" class="{classes}">'.format(
                    open_tag=html_tag,
                    classes=" ".join(found_classes),
                    ids=" ".join(found_ids)
                )
        elif found_ids:
            if item:
                return '<{open_tag} id="{ids}">{value}</{close_tag}>'.format(
                    open_tag=html_tag,
                    value=html.escape(item),
                    close_tag=html_tag,
                    ids=" ".join(found_ids)
                )
            else:
                return '<{open_tag} id="{ids}">'.format(
                    open_tag=html_tag,
                    ids=" ".join(found_ids)
                )
        elif found_classes:
            if item:
                return '<{open_tag} class="{classes}">{value}</{close_tag}>'.format(
                    open_tag=html_tag,
                    value=html.escape(item),
                    close_tag=html_tag,
                    classes=" ".join(found_classes),
                )
            else:
                return '<{open_tag} class="{classes}">>'.format(
                    open_tag=html_tag,
                    classes=" ".join(found_classes),
                )
        else:
            if item:
                return "<{open_tag}>{value}</{close_tag}>".format(
                    open_tag=tag,
                    value=item,
                    close_tag=tag
                )
            else:
                return "<{open_tag}>".format(
                    open_tag=tag,
                )

    # Recursive helper
    def check_item(self, item, tag=None):
        if not isinstance(item, list):
            # print("It is not a list")
            if tag:
                self.output += self.format_string_with_open_and_close_tags(tag=tag, item=item)
            else:  # If it is first run and item is not a list for example ({'p': 'title'})
                for item_tag_value in item.items():
                    item_tag, item_value = item_tag_value[0], item_tag_value[1]
                    self.check_item(item_value, tag=item_tag)
        else:
            # We have list so add ul and li tags
            # print("It is a list")
            if tag:
                self.output += self.format_string_with_open_and_close_tags(tag=tag, item=None)
            self.output += "<ul>"
            for item in item:
                self.output += '<li>'
                for item_tag_value in item.items():
                    item_tag, item_value = item_tag_value[0], item_tag_value[1]
                    self.check_item(item_value, tag=item_tag)
                self.output += '</li>'
            self.output += "</ul>"
            if tag:
                self.output += "</{tag}>".format(tag=self.extract_html_tag_from_tag_key(tag))

    def convert_json_to_html(self):
        with open(self.source_file) as file:
            # Preserving keys ordering with OrderedDict
            # TODO Add try except expression for valid json checking
            source_json = json.loads(file.read(), object_pairs_hook=OrderedDict)
            self.check_item(source_json)
        print(self.output)
        return self.output

if __name__ == '__main__':
    renderer = JSONtoHTMLConverter(source_file=None)
    renderer.convert_json_to_html()
