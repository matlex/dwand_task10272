import unittest

from json_to_html_converter import JSONtoHTMLConverter


class TestConverterTask4_1(unittest.TestCase):
    def setUp(self):
        self.converter = JSONtoHTMLConverter(source_file="task4_1_source.json")

    def test_converter_returns_correct_html(self):
        html = self.converter.convert_json_to_html()
        self.assertEqual('<ul><li>'
                         '<span>Title#1</span>'
                         '<content><ul><li><p>Example1</p><header>header1</header></li></ul></content>'
                         '</li>'
                         '<li><div>div1</div></li></ul>', html)


class TestConverterTask4_2(unittest.TestCase):
    def setUp(self):
        self.converter = JSONtoHTMLConverter(source_file="task4_2_source.json")

    def test_converter_returns_correct_html(self):
        html = self.converter.convert_json_to_html()
        self.assertEqual('<p>hello1</p>', html)


class TestConverterTask4_3(unittest.TestCase):
    def setUp(self):
        self.converter = JSONtoHTMLConverter(source_file="task4_3_source.json")

    def test_converter_returns_correct_html(self):
        html = self.converter.convert_json_to_html()
        self.assertEqual('<p><ul><li><p>Example1</p><header>header1</header></li><li><p>Example2</p><header>header2</header></li></ul></p>', html)


class TestConverterTask5_1(unittest.TestCase):
    def setUp(self):
        self.converter = JSONtoHTMLConverter(source_file="task5_1_source.json")

    def test_converter_returns_correct_html(self):
        html = self.converter.convert_json_to_html()
        self.assertEqual('<p id="my-id" class="my-class">hello</p><p class="my-class1 my-class2">example&lt;a&gt;asd&lt;/a&gt;</p>', html)


class TestConverterTask5_2(unittest.TestCase):
    def setUp(self):
        self.converter = JSONtoHTMLConverter(source_file="task5_2_source.json")

    def test_converter_returns_correct_html(self):
        html = self.converter.convert_json_to_html()
        self.assertEqual('<p id="my-id" class="my-class"><ul><li><p>Example1</p><header>header1</header></li><li><p>Example2</p><header>header2</header></li></ul></p><p class="my-class1 my-class2">example&lt;a&gt;asd&lt;/a&gt;</p>', html)

# Здесь должны быть протестированы функции extract_classes_from_tag, extract_ids_from_tag, extract_html_tag_from_tag_key
# ... на корректность извлечения тегов посредством регекса.

# Далее можно было бы написать кейсы на тестирование функции которая формирует открытие\закрытие хтмл тегов format_string_with_open_and_close_tags
